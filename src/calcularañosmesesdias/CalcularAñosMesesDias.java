package calcularañosmesesdias;

/**
 * Copyright [2016] [Freddy E. Aparicio M.] Licensed under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Developer: Freddy E. Aparicio M. 
 * 
 */

/**
 * Esta clase tienen el modelo y el metodo main
 * para la ejecución del programa. El codigo fuente también se puede observar
 * desde el siguiente repositorio: A nivel general:
 * https://RepositorioFreddy@bitbucket.org/RepositorioFreddy/calculoedaddepersona.git
 * A nivel de codigo:
 * https://bitbucket.org/RepositorioFreddy/calculoedaddepersona/src
 *
 * @author Freddy E. Aparicio M.
 * @version 1.0
 *
 */
public final class CalcularAñosMesesDias {

    /**
     * Representa el dia de nacimiento dentro de la fecha
     */
    private int dia_nacimiento;
    /**
     * Representa el mes de nacimiento dentro de la fecha
     */
    private int mes_nacimiento;
    /**
     * Representa el mes de nacimiento dentro de la fecha
     */
    private int anio_nacimiento;
    /**
     * Representa el dia limite al cual se quiere calcular dias totales
     */
    private int dia_limiteDeCalculo;
    /**
     * Representa el mes limite al cual se quiere calcular meses totales
     */
    private int mes_limiteDeCalculo;
    /**
     * Representa el años limite al cual se quiere calcular anños totales
     */
    private int anio_limiteDeCalculo;

    /**
     * Almacena el resutado de dias totales
     */
    private int dia_resultado = 0;
    /**
     * Almacena el resutado de meses totales
     */
    private int mes_resultado = 0;
    /**
     * Almacena el resutado de años totales
     */
    private int anio_resultado = 0;

    /**
     * Objeto de tipo Scanner para la lectura de datos por consola
     * debido a su reutilizacion no es necesario crear mas de un objeto de
     * este tipo lo cual da para la declaracion por: Nombre cualificado de clase
     */
    java.util.Scanner lectura = new java.util.Scanner(System.in);

    /**
     * Imprime el mesanje informativo sobre el programa; puede llegar a
     * repetirse al digitar una fecha incorrecta.
     */
    private void ImprimirMensajeInformativo() {
        System.out.println("----------------------------------------"
                + "-------------------------");
        System.out.println("| El programa funciona digitando primero la fecha"
                + " de nacimiento \n| y despues la fecha de limite para \n| "
                + "su calculo total de años, meses y dias transcurridos.");
        System.out.println("----------------------------------------"
                + "-------------------------");
    }

    /**
     * Inicia el programa.
     */
    private void InicioDePrograma() {
        ImprimirMensajeInformativo();
        RegistroFechaNacimiento();
        RegistroFechaLimite();
        ValidarFechaNacimientoYLimite();
    }

    /**
     * Se leen y se asignan los datos de la fecha de nacimiento correspondiente
     * al día, mes y año.
     */
    private void RegistroFechaNacimiento() {
        System.out.println("Por favor digite la fecha de nacimiento:");
        System.out.print("dia: ");
        this.dia_nacimiento = lectura.nextInt();
        System.out.print("mes: ");
        this.mes_nacimiento = lectura.nextInt();
        System.out.print("año: ");
        this.anio_nacimiento = lectura.nextInt();
    }

    /**
     * Se asignan los datos de la fecha límite hasta la cual se quiere realizar
     * el cálculo de la edad en días, meses y años.
     */
    private void RegistroFechaLimite() {
        System.out.println("Por favor digite la fecha limite:");
        System.out.print("dia: ");
        this.dia_limiteDeCalculo = lectura.nextInt();
        System.out.print("mes: ");
        this.mes_limiteDeCalculo = lectura.nextInt();
        System.out.print("año: ");
        this.anio_limiteDeCalculo = lectura.nextInt();
    }

    /**
     * Se valida que la fecha de nacimiento y limite sea valida.
     */
    private void ValidarFechaNacimientoYLimite() {
        final String error_diasMesesAnios = "ERROR";

        if (anio_nacimiento > anio_limiteDeCalculo) {
            System.out.println(error_diasMesesAnios);
            System.out.println("\nSe digito el año de nacimiento mayor al del "
                    + "limite; debe iniciar el programa nuevamente.");

        } else if ((dia_nacimiento == 31 || dia_limiteDeCalculo == 31)
                && (mes_nacimiento != 1 || mes_nacimiento != 3
                || mes_nacimiento != 5 || mes_nacimiento != 7
                || mes_nacimiento != 8 || mes_nacimiento != 10
                || mes_nacimiento != 12
                || mes_limiteDeCalculo != 1
                || mes_limiteDeCalculo != 3 || mes_limiteDeCalculo != 5
                || mes_limiteDeCalculo != 7 || mes_limiteDeCalculo != 8
                || mes_limiteDeCalculo != 10 || mes_limiteDeCalculo != 12)) {
            System.out.println(error_diasMesesAnios);
            System.out.println("\nSe digito un mes con numero de "
                    + "días incorrectos; debe iniciar el programa nuevamente.");

        } else if (mes_nacimiento > 12 || mes_nacimiento < 0
                || mes_limiteDeCalculo > 12 || mes_limiteDeCalculo < 0) {
            System.out.println(error_diasMesesAnios);
            System.out.println("\nSe digito un numero de meses "
                    + "incorrectos; debe iniciar el programa nuevamente.");

        } else if (dia_nacimiento > 31 || dia_nacimiento < 0
                || dia_limiteDeCalculo > 31 || dia_limiteDeCalculo < 0) {
            System.out.println(error_diasMesesAnios);
            System.out.println("\nSe digito un numero de dias "
                    + "incorrectos; debe iniciar el programa nuevamente.");

        } else if ((dia_nacimiento > 28 || dia_limiteDeCalculo > 28)
                && (mes_nacimiento == 2 || mes_limiteDeCalculo == 2)) {
            System.out.println(error_diasMesesAnios);
            System.out.println("\nSe digito el mes de febrero "
                    + "con un numero de dias invalidos; debe iniciar el "
                    + "programa nuevamente.");
        } else {
            CalcularDiasMesesAniosTranscurridos();

        }

    }

    /**
     * Calcula y muestra los años, meses y días que tiene la pesona desde su
     * fecha de nacimiento hasta la fecha limite.
     */
    private void CalcularDiasMesesAniosTranscurridos() {
        CalcularAniosTranscurridos();
        CalcularMesesTranscurridos();
        CalcularDiasTranscurridos();
        MostrarResultado();
    }

    /**
     * Calcula años transurridos entre el año de nacimiento y el año limite
     */
    private void CalcularAniosTranscurridos() {
        anio_resultado = anio_limiteDeCalculo - anio_nacimiento;
        if (mes_nacimiento > mes_limiteDeCalculo
                || (mes_nacimiento == mes_limiteDeCalculo
                && dia_limiteDeCalculo < dia_nacimiento)) {
            anio_resultado = anio_resultado - 1;

            if (anio_resultado == -1) {
                anio_resultado = 0;
            }

        }

    }

    /**
     * Calcula meses transurridos entre el meses de nacimiento y el meses limite
     */
    private void CalcularMesesTranscurridos() {
        mes_resultado = mes_limiteDeCalculo - mes_nacimiento;
        if (mes_resultado < 0) {
            mes_resultado = 12 + mes_resultado;

            if (dia_nacimiento > dia_limiteDeCalculo) {
                mes_resultado = mes_resultado - 1;
            }

        }

        if (mes_nacimiento == mes_limiteDeCalculo && dia_nacimiento > dia_limiteDeCalculo) {
            mes_resultado = 11;
        }
    }

    /**
     * Calcula dias transurridos entre el dias de nacimiento y el dias limite
     */
    private void CalcularDiasTranscurridos() {
        dia_resultado = dia_limiteDeCalculo - dia_nacimiento;

        if (dia_nacimiento == 31 && (mes_nacimiento == 1 || mes_nacimiento == 3
                || mes_nacimiento == 5 || mes_nacimiento == 7 || mes_nacimiento == 8
                || mes_nacimiento == 10 || mes_nacimiento == 12)) {
            if (dia_resultado < 0) {
                dia_resultado = 31 + dia_resultado;
            }
        } else if (dia_nacimiento == 28 && mes_nacimiento == 2) {
            if (dia_resultado < 0) {
                dia_resultado = 28 + dia_resultado;
            }
        }

        if (dia_resultado < 0) {
            dia_resultado = 30 + dia_resultado;
        }
    }

    /**
     * Retorna años que tiene la persona
     *
     * @return Retorna el valor de años que tiene la persona
     */
    private int ResultadoAnios() {
        return anio_resultado;
    }

    /**
     * Retorna meses que tiene la persona
     *
     * @return Retorna el valor de meses que tiene la persona
     */
    private int ResultadoMeses() {
        return mes_resultado;
    }

    /**
     * Retorna dias que tiene la persona
     *
     * @return Retorna el valor de dias que tiene la persona
     */
    private int ResultadoDias() {
        return dia_resultado;
    }

    /**
     *
     * Muestra el resultado aglomerando los años, meses y días que tiene la
     * pesona desde su fecha de nacimiento hasta la fecha limite.
     */
    public void MostrarResultado() {
        System.out.println("-------------------------------------------------");
        System.out.println("Se calculo días , meses y años transcurridos "
                + "los cuales son:");
        System.out.println("dias: " + ResultadoDias());
        System.out.println("meses: " + ResultadoMeses());
        System.out.println("años: " + ResultadoAnios());
    }

    /**
     * Metodo main. Interno dentro de la clase modelo lógico.
     *
     * @param args array por defecto.
     */
    public static void main(String[] args) {
        CalcularAñosMesesDias nuevocalculo = new CalcularAñosMesesDias();
        nuevocalculo.InicioDePrograma();
    }
}
